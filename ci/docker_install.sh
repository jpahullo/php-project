#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && [[ ! -e /.dockerinit ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
apt-get update -yqq
apt-get install git libxml2 libxml2-dev php5-xdebug zlib1g-dev -yqq

#pear channel-update pear.php.net
#pear upgrade PEAR
#pecl channel-update pecl.php.net
#pecl upgrade
#pecl install 'xdebug'
#
#XDEBUG_SO="$(find '/usr/lib/php5/' -name 'xdebug.so' \
#    | head -n 1)"
#echo "; configuration for php XDebug module
#; priority=20
#zend_extension=${XDEBUG_SO}" > "${MODS_CONF_PATH}/xdebug.ini"
#test -n "$(command  -v php5enmod)" && command php5enmod 'xdebug'
#
#test -e '/etc/init.d/php5-fpm' && command service 'php5-fpm' 'restart'
#test -e '/etc/init.d/apache2' && command service 'apache2' 'force-reload'

# Install composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === 'e115a8dc7871f15d853148a7fbac7da27d6c0030b848d9b3dc09e2a0388afed865e6a3d6b3c0fad45c48e2b5fc1196ae') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php --filename=composer --install-dir=/usr/local/bin
php -r "unlink('composer-setup.php');"

# Install phpunit, the tool that we will use for testing
#curl -Lo /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
#chmod +x /usr/local/bin/phpunit

# Install mysql driver
# Here you can install any other extension that you need
#docker-php-ext-install pdo_mysql
docker-php-ext-install dom
docker-php-ext-install zip
docker-php-ext-install xdebug

php -i

composer install
