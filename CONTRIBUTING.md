# Contribution Guidelines

Put here your contribution guidelines, including among others:

* How to name your commits.
* When to make a new issue.
* How to build merge requests.
* Peer reviewing workflow.

But contribute please!
