<?php
/*
    A Simple PHP Project
    Copyright (C) 2016 Jordi Pujol-Ahulló <jpahullo@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once(__DIR__ . '/../vendor/autoload.php');
chdir(__DIR__);
echo "PWD: " . __DIR__ . PHP_EOL;

function autoloader($class)
{
    if (file_exists(__DIR__.'/../'.$class.'.php')) {
        require(__DIR__.'/../'.$class.'.php');
        return true;
    }
    return false;
}
spl_autoload_register('autoloader');

//Nothing else required
