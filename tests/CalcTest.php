<?php
/*
    A Simple PHP Project
    Copyright (C) 2016 Jordi Pujol-Ahulló <jpahullo@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

use PHPUnit\Framework\TestCase;

class CalcTest extends TestCase
{
    protected $calc;

    public function setUp()
    {
        $this->calc = new Calc();
    }
    
    /**
     * @dataProvider addProvider
     */
    public function testAdd($a, $b, $expected)
    {
        $this->assertEquals($expected, $a + $b);
    }

    public function addProvider()
    {
        return [
            [0, 0, 0],
            [0, 2, 2],
            [2, 0, 2],
        ];
    }
}
