# PHP Project Example

This is a sample PHP project for testing purposes.

# Author

Jordi Pujol-Ahulló <jpahullo@gmail.com>

# License

[GPL](https://www.gnu.org/licenses/gpl-3.0.txt)
